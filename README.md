# Snapdragon Explorer

A chart to categorize 64-bit Qualcomm Snapdragon SoCs by the number of big+little CPU cores of each Cortex generation.

## SoC Models

Big+Little Cores|A57 |A72 |Kryo '16|A73<br>(Kryo 2xx) |A75<br>(Kryo 3xx) |A76<br>(Kryo 4xx) |A77<br>(Kryo 5xx) |A78<br>(Kryo 6xx)
------|----|----|----|----|----|----|----|----
1X+3+4|    |    |    |    |    |    |    |888
4+4   |**810**|652<br>653    |    |**835**<br>662<br>665<br>632<br>660<br>636<br>460|**845**<br>850|**855**<br>855+<br>860<br>8c[x,x2]<br>SQ1<br>SQ2|**865**<br>865+<br>870|780G
2+6   |    |    |    |670|710<br>712|765[G]<br>768G<br>720G<br>730[G]<br>732G<br>675<br>678<br>480<br>7c|750G<br>690||
2+4   |808 |650    |
2+2   |    |    |**820**<br>821    |

## Notes

- All data from Wikipedia ([1](https://en.wikipedia.org/wiki/List_of_Qualcomm_Snapdragon_processors), [2](https://en.wikipedia.org/wiki/Kryo)).
- Not considering clock frequency, process node, GPU, DPU, ISP, connectivity, etc.
- SoCs with 1+3+4 Prime core pattern are grouped as just 4+4, because other 4+4 may be clocked higher, and the chart ignores clocks (e.g. 855 vs SQ2)
- SoCs with only small cores omitted, as there's no suitable column
